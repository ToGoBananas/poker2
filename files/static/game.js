'use strict';

var winners  = document.getElementsByClassName("winner");
var losers = document.getElementsByClassName("loser");
var points = document.getElementById("points");
var attempts = document.getElementById("attempts");

for (var i=0; i<winners.length; ++i) {
    winners[i].onclick = function() {
        points.value = Number(points.value) + 1;
        document.getElementById("mainButton").click();
    };
}

for (var i=0; i<losers.length; ++i) {
    losers[i].onclick = function() {
        points.value = Number(points.value) - 1;
        attempts.value = Number(attempts.value) - 1;
        document.getElementById("mainButton").click();
    };
}