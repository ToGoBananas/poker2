from django.db import models


class Record(models.Model):
    user = models.CharField(max_length=30)
    points = models.IntegerField(default=0)
    difficulty = models.CharField(max_length=10)
    time = models.DateTimeField()

    def __str__(self):
        return self.user, self.difficulty