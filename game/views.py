from django.shortcuts import render, HttpResponseRedirect
from django.core.urlresolvers import reverse
from game.models import Record
from game.alg import poker, deal
import datetime


def index(request):
    return render(request, 'index.html', )


def game(request):
    hands = []

    try:
        level = request.POST['level']
    except KeyError:
        return render(request, 'index.html', {'time': True})

    try:
        points = request.POST['points']
    except KeyError:
        points = 0

    if level == "Easy":
        hands = deal(2, 5)
        winner = poker(hands)
    elif level == "Medium":
        hands = deal(3, 5)
        winner = poker(hands)
    else:
        hands = deal(5, 5)
        winner = poker(hands)

    return render(request, 'games/game.html', {'hands': hands, 'winner': winner,
                                               'points': points, 'level': level})


def game2(request):
    level = request.POST['level']

    try:
        points, attempts = request.POST['points'], int(request.POST['attempts'])
    except KeyError:
        points, attempts = 0, 3

    if attempts == 0:
        return render(request, 'record.html', {'record': points, 'level': level, 'attempts': attempts})

    if level == "Easy":
        hands = deal(2, 5)
        winner = poker(hands)
    elif level == "Medium":
        hands = deal(3, 5)
        winner = poker(hands)
    else:
        hands = deal(5, 5)
        winner = poker(hands)

    return render(request, 'games/game2.html', {'hands': hands, 'winner': winner, 'attempts': attempts,
                                                'points': points, 'level': level})


def record(request):
    record, level = int(request.POST['record']), request.POST['level']
    return render(request, 'record.html', {'record': record, 'level': level})


def results(request):
    if request.method == 'POST':
        rec = Record()
        rec.points, rec.user = int(request.POST['record']), request.POST['user']
        rec.time, rec.difficulty = datetime.datetime.now(), request.POST['level']
        rec.save()
        return HttpResponseRedirect(reverse('results', ))

    records = Record.objects.order_by('-points')
    return render(request, 'results.html', {'records': records})