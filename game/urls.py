from django.conf.urls import patterns, include, url

from game import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^game/$', views.game, name='game'),
    url(r'^game2/$', views.game2, name='game2'),
    url(r'^record/$', views.record, name='record'),
    url(r'^results/$', views.results, name='results'),
)
