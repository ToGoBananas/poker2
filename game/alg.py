import random


def poker(hands):
    return allmax(hands, key=hand_rank)


def allmax(iterable, key=lambda x: x):
    maxi = max(iterable, key=key)
    return [element for element in iterable if key(element) == key(maxi)]


def hand_rank(hand):
    groups = group(['--23456789tjqk1'.index(r) for s, r in hand])
    counts, ranks = unzip(groups)
    if ranks == (14, 5, 4, 3, 2):
        ranks = (5, 4, 3, 2, 1)
    straight = len(ranks) == 5 and max(ranks)-min(ranks) == 4
    flush = len(set([s for r, s in hand])) == 1
    return (
        9 if (5, ) == counts else
        8 if straight and flush else
        7 if (4, 1) == counts else
        6 if (3, 2) == counts else
        5 if flush else
        4 if straight else
        3 if (3, 1, 1) == counts else
        2 if (2, 2, 1) == counts else
        1 if (2, 1, 1, 1) == counts else
        0), ranks


def group(items):
    groups = [(items.count(x), x) for x in set(items)]
    return sorted(groups, reverse=True)


def unzip(pairs):
    return zip(*pairs)


def deal(numhands, n=5, deck=[r+s for r in 'shdc' for s in '23456789tjqk1']):
    shuffle(deck)
    deck = iter(deck)
    return [[next(deck) for card in range(n)] for hand in range(numhands)]


def shuffle(deck):
    n = len(deck)
    for i in range(n-1):
        j = random.randrange(i, n)
        deck[i], deck[j] = deck[j], deck[i]
